<?php

namespace CYINT\ComponentsPHP\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use CYINT\ComponentsPHP\Classes\ViewMessage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

abstract class MasterController extends Controller implements IApplicationControllerInterface
{ 
    protected $viewParams;

    protected function handleErrors($callback, $redirect = null, $_render = 'HTML')
    {                 
        $Session = $this->get('session');
        $messages = empty($Session->get('messages')) ? [] : $Session->get('messages');
     
        try
        {            
            $this->controllerHook($Session, $messages);
            return $callback($Session, $messages);
        }
        catch( \Exception $Ex)
        {
            $env = $this->container->get('kernel')->getEnvironment();
            $messages[] = ViewMessage::constructMessage($Ex->getMessage(), 'danger');
            if($env == 'dev')
                die(print_r($Ex->getMessage() . ' ' .  $Ex->getFile() . ' ' . $Ex->getLine()));

            if($_render != 'JSON')
            {
                $redirects = empty($Session->get('redirect_count')) ? 0 : 1;
                if(!empty($redirect) && $redirects < 1)
                {
                    $Session->set('messages', $messages);
                    $Session->set('redirect_count', 1);
                    return $this->redirect($redirect);
                }
                else
                {
                    $Session->set('redirect_count', 0);
                    throw $Ex;
                }
            }
            else         
            {

                $Session->set('messages', null);     
                return $this->sendJsonResponse(['success'=>false,'data'=>$messages]);           
            }
        }
    }

    protected function renderRoute($template, $params, $_render = 'HTML')
    {
        $this->preRenderRoute();

        if(!empty($this->viewParams))
            $params = array_merge($params, $this->viewParams);

        if($_render == 'HTML')
        {
            return $this->render($template, $params);
        }
        elseif($_render == 'JSON')
        {       
            $JsonFormatter = $this->get('app.jsonformatter');
            $params = $JsonFormatter->collectionToJson($params, true); 						             
			return $this->sendJsonResponse(['success' => true, 'data' => $params]);
        }   
        else
        	return $this->sendDynamicResponse($_render, $template, $params);
    }

    protected function authenticationHelperUser($User)
    {   // Here, "public" is the name of the firewall in your security.yml
        $token = new UsernamePasswordToken($User, $User->getPassword(), "main", $User->getRoles());

        // For older versions of Symfony, use security.context here
        $this->get("security.token_storage")->setToken($token);

	return ['User'=> $User, 'token'=>$token];
    }

    protected function authenticationHelper($username, $password)
    {
        $UserManager = $this->get('app.user_manager');      
	$User = $UserManager->findUserByUsername($username);                

	if(empty($User))
	    throw new \Exception('An account associated with that email could not be found.');
	   
	if(empty($User->isEnabled()))                    
	    throw new \Exception('Before logging in with this account, you must confirm your email address by clicking on the link in the confirmation email. If you did not receive it, try checking your spam folder.');                    
						   
	$encoder_service = $this->get('security.encoder_factory');
	$encoder = $encoder_service->getEncoder($User);

	if (!$encoder->isPasswordValid($User->getPassword(), $password, $User->getSalt()))                        
	    throw new \Exception('We could not authenticate you with the credentials you provided.');               

        return $this->authenticationHelperUser($User);	
    }

    protected function controllerHook($Session, $messages)
    {
        return;
    }

    protected function sendExcelFileResponse($Request, $filepath, $filename)
    {
        $response = new BinaryFileResponse($filepath);
        // Redirect output to a clients web browser (Excel5)
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $filename .'.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->prepare($Request);
        $response->sendHeaders();
   
        BinaryFileResponse::trustXSendfileTypeHeader();
        return $response;
    }

	protected function sendJsonResponse($json)
	{
		$JsonResponse = new JsonResponse();
		$JsonResponse->setData($json);
		return $JsonResponse;
	}

	protected function sendDynamicResponse($_render, $template, $params)
	{
		return new Response(
			 $template, 200, [
				 'Content-Type' => $_render,
				 $params
			 ]
		);
	}
}
